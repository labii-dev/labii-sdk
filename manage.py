""" internal package management """
import sys
import os
from labii_sdk_core.sdk import prepare_usage, print_blue, print_red, print_green, print_yellow, push_code, merge_requests, get_optional_arg, get_account_region, get_version
from labii_sdk_core.whl import install_whl

def main():
	"""
		the main function
	"""
	# update here for name of the function
	actions = []

	# update here for arguments
	usage = prepare_usage(actions)

	if len(sys.argv) < 2:
		print_blue(usage)
		sys.exit()

	# get action
	action = sys.argv[1]
	if len(sys.argv) >= 3:
		para = sys.argv[2]

	# start function
	if action == "push":
		account = get_optional_arg("--account-", sys.argv, "labii-test")
		region = get_account_region(account)
		name = f"{account}-sdk"
		# common function
		if "--skiplogin" in sys.argv:
			push_code(False, name, region)
		else:
			push_code(True, name, region)
	elif action == "merge":
		merge_requests()
	elif action == "install_whl":
		install_whl("labii_sdk_core")
	elif action == "submit":
		print(usage)
	elif action == "version":
		print_yellow(get_version())
	elif action == "test":
		#os.system("python -m unittest test_api.py")
		os.system("python -m unittest test_labii.py")
	elif action == "build":
		os.system("python -m build")
	elif action == "upload":
		as_test = input("Should I upload to testpypi? [Y/n/b] ")
		if as_test == "n":
			os.system("python -m twine upload --skip-existing dist/*")
		elif as_test == "b":
			os.system("python -m twine upload --repository testpypi --skip-existing dist/*")
			os.system("python -m twine upload --skip-existing dist/*")
		else:
			os.system("python -m twine upload --repository testpypi --skip-existing dist/*")
	elif action == "deploy":
		os.system("python manage.py merge")
		os.system("python manage.py build")
		os.system("python manage.py upload")
	else:
		print_red(f"Error: Action ({action}) is not recognizable!")
		print_blue(usage)
		sys.exit()
	print_green("Done!")

if __name__ == "__main__":
	main()
